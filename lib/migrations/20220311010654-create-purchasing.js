'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Purchasings', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        default: Sequelize.UUIDV4
      },
      orderCode: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "order_code"
      },
      userId: {
        type: Sequelize.UUID,
        allowNull: false,
        field: "user_id"
      },
      totalPrice: {
        type: Sequelize.BIGINT,
        allowNull: false,
        field: "total_price"
      },
      status: {
        type: Sequelize.ENUM(["PENDING", "PAID", "COMPLETED", "REJECTED"]),
        defaultValue: "PENDING"
      },
      receipt: {
        type: Sequelize.STRING,
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: "created_at",
        defaultValue: Sequelize.NOW
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        field: "updated_at",
        defaultValue: Sequelize.NOW
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
        field: "deleted_at"
      }
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Purchasings');
  }

};