"use strict";

var _express = _interopRequireDefault(require("express"));

var _PurchasingController = require("../controllers/PurchasingController");

var _authentication = require("../middlewares/authentication");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express.default.Router();

const purchasingController = new _PurchasingController.PurchasingController();
router.get("/", _authentication.authenticate, purchasingController.index);
router.post("/", _authentication.authenticateUser, purchasingController.store);
router.post("/:id/receipt", _authentication.authenticateUser, purchasingController.uploadReceipt);
router.patch("/:id/status", _authentication.authenticateAdmin, purchasingController.setPurchaseStatus);
module.exports = router;