"use strict";

var _express = _interopRequireDefault(require("express"));

var _StatisticController = require("../controllers/StatisticController");

var _authentication = require("../middlewares/authentication");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express.default.Router();

const statisticController = new _StatisticController.StatisticController();
router.get("/sold-item", _authentication.authenticateAdmin, statisticController.soldItem);
router.get("/loyal-customer", _authentication.authenticateAdmin, statisticController.loyalCustomer);
module.exports = router;