"use strict";

var _express = _interopRequireDefault(require("express"));

var _ItemController = require("../controllers/ItemController");

var _authentication = require("./../middlewares/authentication");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express.default.Router();

const itemController = new _ItemController.ItemController();
router.get("/", itemController.index);
router.get("/:id", itemController.show);
router.post("/", _authentication.authenticateAdmin, itemController.store);
router.put("/:id", _authentication.authenticateAdmin, itemController.update);
router.delete("/:id", _authentication.authenticateAdmin, itemController.destroy);
module.exports = router;