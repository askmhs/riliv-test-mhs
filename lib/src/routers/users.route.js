"use strict";

var _express = _interopRequireDefault(require("express"));

var _UserController = require("../controllers/UserController");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express.default.Router();

const userController = new _UserController.UserController();
router.post("/", userController.signUp);
router.post("/login", userController.login);
module.exports = router;