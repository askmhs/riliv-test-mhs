"use strict";

var _express = _interopRequireDefault(require("express"));

var _items = _interopRequireDefault(require("./items.route"));

var _users = _interopRequireDefault(require("./users.route"));

var _purchasings = _interopRequireDefault(require("./purchasings.route"));

var _statistics = _interopRequireDefault(require("./statistics.route"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express.default.Router();

router.use("/items", _items.default);
router.use("/users", _users.default);
router.use("/purchasings", _purchasings.default);
router.use("/statistics", _statistics.default);
module.exports = router;