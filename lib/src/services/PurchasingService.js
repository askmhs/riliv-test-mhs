"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PurchasingService = void 0;

var _response = require("express/lib/response");

var _sequelize = require("sequelize");

var _models = require("../../models");

var _item = require("../../models/item");

var _purchasing = require("../../models/purchasing");

var _purchasingitem = require("../../models/purchasingitem");

class PurchasingService {
  async listPurchase(queries, userId, userRole) {
    const {
      q,
      page = 1,
      perPage = 10
    } = queries;
    const params = {};

    if (userRole === "USER") {
      Object.assign(params, {
        userId
      });
    }

    if (q) {
      Object.assign(params, {
        orderCode: {
          [_sequelize.Op.like]: `%${searchKey}%`
        }
      });
    }

    const offset = (page - 1) * perPage;
    const limit = perPage;
    return _purchasing.Purchasing.findAll({
      where: params,
      order: [["createdAt", "DESC"]],
      offset,
      limit
    });
  }

  async detailPurchase(id, userRole, userId) {
    const params = {
      id
    };

    if (userRole === "USER") {
      Object.assign(params, {
        userId
      });
    }

    const purchase = await _purchasing.Purchasing.findOne({
      where: params,
      include: {
        association: "purchasingItems"
      }
    });

    if (!purchase) {
      throw new Error(`Whoops!, The purchase with ID ${id} couldn't be found!`);
    }

    return purchase;
  }

  async createNewPurchase(data, user) {
    const {
      items
    } = data;
    const {
      id: userId
    } = user;
    const orderCode = `PUR-${new Date().getTime()}`;
    return _models.sequelize.transaction(async () => {
      const purchasing = new _purchasing.Purchasing({
        userId,
        orderCode
      });
      let totalPrice = 0;

      for (let item of items) {
        const validItem = await this.validateItem(item.id);
        await _purchasingitem.PurchasingItem.create({
          itemId: item.id,
          purchasingId: purchasing.id,
          qty: item.qty,
          price: validItem.price
        });
        validItem.stock -= item.qty;
        await validItem.save();
        totalPrice += item.qty * validItem.price;
      }

      purchasing.totalPrice = totalPrice;
      await purchasing.save();
      return _purchasing.Purchasing.findByPk(purchasing.id, {
        include: {
          association: "purchasingItems"
        }
      });
    });
  }

  async validateItem(id, qty) {
    const item = await _item.Item.findByPk(id);

    if (!item) {
      throw new Error(`Whoops!, Item with the ID ${id} couldn't be found`);
    }

    if (item && item.qty < qty) {
      throw new Error(`Whoops!, Item with the ID ${id} didn't have enough stock`);
    }

    return item;
  }

  async uploadReceipt(id, receipt, userId) {
    const purchasing = await _purchasing.Purchasing.findOne({
      where: {
        id,
        userId,
        status: "PENDING"
      }
    });

    if (!purchasing) {
      throw new Error(`Purchase with ID ${id} couldn't be found!`);
    }

    const splitName = receipt.name.split(".");
    const extension = splitName[splitName.length - 1];
    const fileName = `uploads/${new Date().getTime()}.${extension}`;
    const path = `${__dirname}/../../${fileName}`;
    await receipt.mv(path);
    purchasing.receipt = fileName;
    await purchasing.update();
    return purchasing;
  }

  async setStatus(orderId, status) {
    const allowedStatus = ["PAID", "COMPLETED", "REJECTED"];

    if (!allowedStatus.includes(status)) {
      throw new Error(`Whoops!, You are not allowed to set purchase status to ${status}`);
    }

    const purchase = await _purchasing.Purchasing.findByPk(orderId);

    if (!purchase) {
      throw new Error(`Whoops!, The purchase data couldn't be found`);
    }

    purchase.status = status.toUpperCase();
    await purchase.update();
    return purchase;
  }

}

exports.PurchasingService = PurchasingService;