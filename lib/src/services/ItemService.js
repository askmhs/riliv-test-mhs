"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ItemService = void 0;

var _sequelize = require("sequelize");

var _item = require("../../models/item");

class ItemService {
  async createNewItem(data) {
    return _item.Item.create(data);
  }

  async listItem(searchKey) {
    let params = {};

    if (searchKey) {
      Object.assign(params, {
        name: {
          [_sequelize.Op.like]: `%${searchKey}%`
        }
      });
    }

    return _item.Item.findAll({
      where: params,
      order: [["createdAt", "DESC"]]
    });
  }

  async detailItem(id) {
    return _item.Item.findByPk(id);
  }

  async updateItem(id, data) {
    const item = await _item.Item.findByPk(id);

    if (!item) {
      throw new Error(`Whoops!, product with ID ${id} couldn't be found`);
    }

    await item.update(data);
    return this.detailItem(id);
  }

  async deleteItem(id) {
    const item = await _item.Item.findByPk(id);

    if (!item) {
      throw new Error(`Whoops!, product with ID ${id} couldn't be found`);
    }

    return item.destroy();
  }

}

exports.ItemService = ItemService;