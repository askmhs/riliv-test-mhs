"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StatisticService = void 0;

var _sequelize = require("sequelize");

var _purchasingitem = require("../../models/purchasingitem");

var _moment = _interopRequireDefault(require("moment"));

var _ = _interopRequireWildcard(require("lodash"));

var _item = require("../../models/item");

var _purchasing = require("../../models/purchasing");

var _user = require("../../models/user");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class StatisticService {
  constructor() {
    _defineProperty(this, "_sortType", void 0);

    this.sortType = ["asc", "desc"];
  }

  get sortType() {
    return this._sortType;
  }

  set sortType(value) {
    this._sortType = value;
  }

  async soldItem(sort, startDate, endDate) {
    if (!this.sortType.includes(sort)) {
      throw new Error(`Whoops!, ${sort} is not a valid param`);
    }

    if (!startDate) {
      startDate = (0, _moment.default)().startOf("day").toISOString();
    } else {
      startDate = (0, _moment.default)(startDate).startOf("day").toISOString();
    }

    if (!endDate) {
      endDate = (0, _moment.default)().endOf("day").toISOString();
    } else {
      endDate = (0, _moment.default)(endDate).endOf("day").toISOString();
    }

    const result = await _purchasingitem.PurchasingItem.findAll({
      where: {
        createdAt: {
          [_sequelize.Op.between]: [startDate, endDate]
        }
      },
      attributes: ["PurchasingItem.item_id", [_sequelize.Sequelize.fn("sum", _sequelize.Sequelize.col("PurchasingItem.qty")), "qty"]],
      include: {
        model: _item.Item,
        attributes: ["id", "name"]
      },
      group: ["PurchasingItem.item_id", "Item.id", "Item.name"]
    });
    return _.chain(result).orderBy("qty", sort).map(object => {
      return {
        qty: object.qty,
        id: object.Item.id,
        name: object.Item.name
      };
    }).value();
  }

  async loyalCustomer(startDate, endDate) {
    if (!startDate) {
      startDate = (0, _moment.default)().startOf("day").toISOString();
    } else {
      startDate = (0, _moment.default)(startDate).startOf("day").toISOString();
    }

    if (!endDate) {
      endDate = (0, _moment.default)().endOf("day").toISOString();
    } else {
      endDate = (0, _moment.default)(endDate).endOf("day").toISOString();
    }

    const result = await _purchasing.Purchasing.findAll({
      where: {
        createdAt: {
          [_sequelize.Op.between]: [startDate, endDate]
        }
      },
      attributes: ["Purchasing.user_id", [_sequelize.Sequelize.fn("COUNT", _sequelize.Sequelize.col("Purchasing.user_id")), "orderCount"]],
      include: {
        model: _user.User,
        attributes: ["id", "name"]
      },
      group: ["Purchasing.user_id", "User.id", "User.name"]
    });
    return _.chain(result).orderBy("orderCount", "desc").map(object => {
      object = object.toJSON();
      return {
        id: object.User.id,
        name: object.User.name,
        orderCount: object.orderCount
      };
    }).value();
  }

}

exports.StatisticService = StatisticService;