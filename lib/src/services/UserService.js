"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UserService = void 0;

var _user = require("../../models/user");

var _bcrypt = _interopRequireDefault(require("bcrypt"));

var _Token = require("../utils/Token");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class UserService {
  constructor() {
    _defineProperty(this, "_token", void 0);

    this.token = new _Token.Token();
  }

  get token() {
    return this._token;
  }

  set token(value) {
    this._token = value;
  }

  async createNewUser(data) {
    const salt = await _bcrypt.default.genSalt(10);
    const hashedPassword = await _bcrypt.default.hash(data.password, salt);
    data.password = hashedPassword;
    const user = new _user.User(data);
    const {
      id,
      name,
      role
    } = user;
    const accessToken = await this.token.encode({
      id,
      name,
      role
    });
    user.accessToken = accessToken;
    await user.save();
    return {
      accessToken
    };
  }

  async loginUser(email, password) {
    const user = await _user.User.findOne({
      where: {
        email
      }
    });

    if (!user) {
      throw new Error("Whoops!, Invalid email or password");
    }

    const isPasswordValid = await _bcrypt.default.compare(password, user.password);

    if (!isPasswordValid) {
      throw new Error("Whoops!, Invalid email or password");
    }

    const {
      id,
      name,
      role
    } = user;
    const accessToken = await this.token.encode({
      id,
      name,
      role
    });
    user.accessToken = accessToken;
    await user.save();
    return {
      accessToken
    };
  }

}

exports.UserService = UserService;