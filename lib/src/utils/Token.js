"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Token = void 0;

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class Token {
  constructor() {
    _defineProperty(this, "_privateKey", void 0);

    this.privateKey = process.env.JWT_PRIVATE_KEY || "riliv-test";
  }

  get privateKey() {
    return this._privateKey;
  }

  set privateKey(value) {
    this._privateKey = value;
  }

  async encode(data) {
    return _jsonwebtoken.default.sign(data, this.privateKey, {
      algorithm: "HS256",
      expiresIn: "1h"
    });
  }

  decode(token) {
    return _jsonwebtoken.default.verify(token, this.privateKey, {
      algorithms: "HS256"
    });
  }

}

exports.Token = Token;