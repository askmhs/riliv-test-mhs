"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ResponseBuilder = void 0;

class ResponseBuilder {
  static successResponse(message, data) {
    return {
      success: true,
      message,
      data
    };
  }

  static errorResponse(errors) {
    return {
      success: false,
      message: "Whoops!, An error occurred.",
      errors
    };
  }

}

exports.ResponseBuilder = ResponseBuilder;