"use strict";

var _express = _interopRequireDefault(require("express"));

var _cors = _interopRequireDefault(require("cors"));

var _routers = _interopRequireDefault(require("./routers"));

var _models = _interopRequireDefault(require("./../models"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _user = require("../models/user");

var _item = require("../models/item");

var _purchasing = require("../models/purchasing");

var _purchasingitem = require("../models/purchasingitem");

var _expressFileupload = _interopRequireDefault(require("express-fileupload"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = (0, _express.default)();
app.use((0, _expressFileupload.default)());
app.use(_bodyParser.default.json());
app.use(_routers.default);
app.use((0, _cors.default)());

_models.default.sequelize.authenticate().then(async () => {
  _user.User.init(_models.default.sequelize);

  _item.Item.init(_models.default.sequelize);

  _purchasing.Purchasing.init(_models.default.sequelize);

  _purchasingitem.PurchasingItem.init(_models.default.sequelize);

  _purchasing.Purchasing.associate();

  _purchasingitem.PurchasingItem.associate();

  const port = process.env.PORT || 3000;
  app.listen(port, () => {
    console.log(`Express server listening on port ${port}.`);
  });
}).catch(error => {
  throw error;
});