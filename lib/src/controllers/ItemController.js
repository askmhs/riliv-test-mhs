"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ItemController = void 0;

var _express = require("express");

var _ItemService = require("../services/ItemService");

var _ResponseBuilder = require("../utils/ResponseBuilder");

const itemService = new _ItemService.ItemService();

class ItemController {
  async index(req, res) {
    try {
      const result = await itemService.listItem(req.query.q);
      res.status(200).json(_ResponseBuilder.ResponseBuilder.successResponse("List item", result));
    } catch (error) {
      console.log(error);
      res.status(500).json(_ResponseBuilder.ResponseBuilder.errorResponse(error.message));
    }
  }

  async show(req, res) {
    try {
      const result = await itemService.detailItem(req.params.id);
      res.status(200).json(_ResponseBuilder.ResponseBuilder.successResponse("Detail item", result));
    } catch (error) {
      console.log(error);
      res.status(500).json(_ResponseBuilder.ResponseBuilder.errorResponse(error.message));
    }
  }

  async store(req, res) {
    try {
      const result = await itemService.createNewItem(req.body);
      res.status(200).json(_ResponseBuilder.ResponseBuilder.successResponse("Yeay!, Successfully create a new item", result));
    } catch (error) {
      console.log(error);
      res.status(500).json(_ResponseBuilder.ResponseBuilder.errorResponse(error.message));
    }
  }

  async update(req, res) {
    try {
      const result = await itemService.updateItem(req.params.id, req.body);
      res.status(200).json(_ResponseBuilder.ResponseBuilder.successResponse("Yeay!, Successfully update an item", result));
    } catch (error) {
      console.log(error);
      res.status(500).json(_ResponseBuilder.ResponseBuilder.errorResponse(error.message));
    }
  }

  async destroy(req, res) {
    try {
      const result = await itemService.deleteItem(req.params.id);
      res.status(200).json(_ResponseBuilder.ResponseBuilder.successResponse("Yeay!, Successfully delete an item", result));
    } catch (error) {
      console.log(error);
      res.status(500).json(_ResponseBuilder.ResponseBuilder.errorResponse(error.message));
    }
  }

}

exports.ItemController = ItemController;