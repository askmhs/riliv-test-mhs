"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StatisticController = void 0;

var _sequelize = require("sequelize");

var _StatisticService = require("../services/StatisticService");

var _ResponseBuilder = require("../utils/ResponseBuilder");

const statisticService = new _StatisticService.StatisticService();

class StatisticController {
  async soldItem(req, res) {
    try {
      const {
        sort = "asc",
        startDate,
        endDate
      } = req.query;
      const result = await statisticService.soldItem(sort, startDate, endDate);
      res.status(200).json(_ResponseBuilder.ResponseBuilder.successResponse("Sold item statistic", result));
    } catch (error) {
      console.log(error);
      res.status(500).json(_ResponseBuilder.ResponseBuilder.errorResponse(error.message));
    }
  }

  async loyalCustomer(req, res) {
    try {
      const {
        startDate,
        endDate
      } = req.query;
      const result = await statisticService.loyalCustomer(startDate, endDate);
      res.status(200).json(_ResponseBuilder.ResponseBuilder.successResponse("Loyal customer statistic", result));
    } catch (error) {
      console.log(error);
      res.status(500).json(_ResponseBuilder.ResponseBuilder.errorResponse(error.message));
    }
  }

}

exports.StatisticController = StatisticController;