"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UserController = void 0;

var _express = require("express");

var _UserService = require("../services/UserService");

var _ResponseBuilder = require("../utils/ResponseBuilder");

const userService = new _UserService.UserService();

class UserController {
  async signUp(req, res) {
    try {
      const result = await userService.createNewUser(req.body);
      res.status(200).json(_ResponseBuilder.ResponseBuilder.successResponse("Yeay!, You're successfully registered", result));
    } catch (error) {
      console.log(error);
      res.status(500).json(_ResponseBuilder.ResponseBuilder.errorResponse(error.message));
    }
  }

  async login(req, res) {
    try {
      const {
        email,
        password
      } = req.body;
      const result = await userService.loginUser(email, password);
      res.status(200).json(_ResponseBuilder.ResponseBuilder.successResponse("Yeay!, You're successfully logged in", result));
    } catch (error) {
      console.log(error);
      res.status(500).json(_ResponseBuilder.ResponseBuilder.errorResponse(error.message));
    }
  }

}

exports.UserController = UserController;