"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PurchasingController = void 0;

var _express = require("express");

var _PurchasingService = require("../services/PurchasingService");

var _ResponseBuilder = require("../utils/ResponseBuilder");

const purchasingService = new _PurchasingService.PurchasingService();

class PurchasingController {
  async index(req, res) {
    try {
      const {
        id: userId,
        role: userRole
      } = req.user;
      const result = await purchasingService.listPurchase(req.query, userId, userRole);
      res.status(200).json(_ResponseBuilder.ResponseBuilder.successResponse("List purchase", result));
    } catch (error) {
      console.log(error);
      res.status(500).json(_ResponseBuilder.ResponseBuilder.errorResponse(error.message));
    }
  }

  async show(req, res) {
    try {
      const {
        id: userId,
        role: userRole
      } = req.user;
      const result = await purchasingService.detailPurchase(req.params.id, userId, userRole);
      res.status(200).json(_ResponseBuilder.ResponseBuilder.successResponse("Detail purchase", result));
    } catch (error) {
      console.log(error);
      res.status(500).json(_ResponseBuilder.ResponseBuilder.errorResponse(error.message));
    }
  }

  async store(req, res) {
    try {
      const result = await purchasingService.createNewPurchase(req.body, req.user);
      res.status(200).json(_ResponseBuilder.ResponseBuilder.successResponse("Yeay!, Successfully create a new purchase", result));
    } catch (error) {
      console.log(error);
      res.status(500).json(_ResponseBuilder.ResponseBuilder.errorResponse(error.message));
    }
  }

  async uploadReceipt(req, res) {
    try {
      const result = await purchasingService.uploadReceipt(req.params.id, req.files.receipt, req.user.id);
      res.status(200).json(_ResponseBuilder.ResponseBuilder.successResponse("Yeay!, Successfully upload receipt", result));
    } catch (error) {
      console.log(error);
      res.status(500).json(_ResponseBuilder.ResponseBuilder.errorResponse(error.message));
    }
  }

  async setPurchaseStatus(req, res) {
    try {
      const result = await purchasingService.setStatus(req.params.id, req.body.status);
      res.status(200).json(_ResponseBuilder.ResponseBuilder.successResponse("Yeay!, Successfully update purchase status", result));
    } catch (error) {
      console.log(error);
      res.status(500).json(_ResponseBuilder.ResponseBuilder.errorResponse(error.message));
    }
  }

}

exports.PurchasingController = PurchasingController;