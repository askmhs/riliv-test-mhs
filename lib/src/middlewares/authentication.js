"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.authenticateUser = exports.authenticateAdmin = exports.authenticate = void 0;

var _ResponseBuilder = require("../utils/ResponseBuilder");

var _express = require("express");

var _Token = require("../utils/Token");

const token = new _Token.Token();

const authenticate = (req, res, next) => {
  const accessToken = req.headers.token;

  if (!accessToken) {
    return res.status(401).json(_ResponseBuilder.ResponseBuilder.errorResponse("Whoops!, authentication token is missing"));
  }

  try {
    const decodedToken = token.decode(accessToken);
    req.user = decodedToken;
    next();
  } catch (error) {
    console.log(error);
    return res.status(401).json(_ResponseBuilder.ResponseBuilder.errorResponse("Whoops!, you are not allowed to access this feature"));
  }
};

exports.authenticate = authenticate;

const authenticateUser = (req, res, next) => {
  const accessToken = req.headers.token;

  if (!accessToken) {
    return res.status(401).json(_ResponseBuilder.ResponseBuilder.errorResponse("Whoops!, authentication token is missing"));
  }

  try {
    const decodedToken = token.decode(accessToken);

    if (decodedToken.role !== "USER") {
      return res.status(401).json(_ResponseBuilder.ResponseBuilder.errorResponse("Whoops!, you are not allowed to access this feature"));
    }

    req.user = decodedToken;
    next();
  } catch (error) {
    console.log(error);
    return res.status(401).json(_ResponseBuilder.ResponseBuilder.errorResponse("Whoops!, you are not allowed to access this feature"));
  }
};

exports.authenticateUser = authenticateUser;

const authenticateAdmin = (req, res, next) => {
  const accessToken = req.headers.token;

  if (!accessToken) {
    res.status(401).json(_ResponseBuilder.ResponseBuilder.errorResponse("Whoops!, authentication token is missing"));
  }

  try {
    const decodedToken = token.decode(accessToken);

    if (decodedToken.role !== "ADMIN") {
      return res.status(401).json(_ResponseBuilder.ResponseBuilder.errorResponse("Whoops!, you are not allowed to access this feature"));
    }

    req.user = decodedToken;
    next();
  } catch (error) {
    console.log(error);
    return res.status(401).json(_ResponseBuilder.ResponseBuilder.errorResponse("Whoops!, you are not allowed to access this feature"));
  }
};

exports.authenticateAdmin = authenticateAdmin;