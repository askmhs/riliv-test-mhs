"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PurchasingItem = void 0;

var _sequelize = require("sequelize");

var _item = require("./item");

class PurchasingItem extends _sequelize.Model {
  static init(sequelize) {
    return super.init({
      id: {
        allowNull: false,
        primaryKey: true,
        type: _sequelize.DataTypes.UUID,
        defaultValue: _sequelize.DataTypes.UUIDV4
      },
      purchasingId: {
        type: _sequelize.DataTypes.UUID,
        allowNull: false,
        field: "purchasing_id"
      },
      itemId: {
        type: _sequelize.DataTypes.UUID,
        allowNull: false,
        field: "item_id"
      },
      qty: {
        type: _sequelize.DataTypes.INTEGER,
        allowNull: false
      },
      price: {
        type: _sequelize.DataTypes.BIGINT,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: _sequelize.DataTypes.DATE,
        field: "created_at",
        defaultValue: _sequelize.DataTypes.NOW
      },
      updatedAt: {
        allowNull: false,
        type: _sequelize.DataTypes.DATE,
        field: "updated_at",
        defaultValue: _sequelize.DataTypes.NOW
      },
      deletedAt: {
        allowNull: true,
        type: _sequelize.DataTypes.DATE,
        field: "deleted_at"
      }
    }, {
      sequelize: sequelize,
      tableName: "PurchasingItems"
    });
  }

  static associate() {
    this.belongsTo(_item.Item, {
      foreignKey: "item_id"
    });
  }

}

exports.PurchasingItem = PurchasingItem;