"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Purchasing = void 0;

var _sequelize = require("sequelize");

var _purchasingitem = require("./purchasingitem");

var _user = require("./user");

class Purchasing extends _sequelize.Model {
  static init(sequelize) {
    return super.init({
      id: {
        allowNull: false,
        primaryKey: true,
        type: _sequelize.DataTypes.UUID,
        defaultValue: _sequelize.DataTypes.UUIDV4
      },
      orderCode: {
        type: _sequelize.DataTypes.STRING,
        allowNull: false,
        field: "order_code"
      },
      userId: {
        type: _sequelize.DataTypes.UUID,
        allowNull: false,
        field: "user_id"
      },
      totalPrice: {
        type: _sequelize.DataTypes.BIGINT,
        allowNull: false,
        field: "total_price"
      },
      status: {
        type: _sequelize.DataTypes.ENUM(["PENDING", "PAID", "COMPLETED", "REJECTED"]),
        defaultValue: "PENDING"
      },
      receipt: {
        type: _sequelize.DataTypes.STRING,
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: _sequelize.DataTypes.DATE,
        field: "created_at",
        defaultValue: _sequelize.DataTypes.NOW
      },
      updatedAt: {
        allowNull: false,
        type: _sequelize.DataTypes.DATE,
        field: "updated_at",
        defaultValue: _sequelize.DataTypes.NOW
      },
      deletedAt: {
        allowNull: true,
        type: _sequelize.DataTypes.DATE,
        field: "deleted_at"
      }
    }, {
      sequelize: sequelize,
      tableName: "Purchasings"
    });
  }

  static associate() {
    this.hasMany(_purchasingitem.PurchasingItem, {
      as: "purchasingItems",
      foreignKey: "purchasing_id",
      hooks: true
    });
    this.belongsTo(_user.User, {
      foreignKey: "user_id"
    });
  }

}

exports.Purchasing = Purchasing;