"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.User = void 0;

var _sequelize = require("sequelize");

var _purchasing = require("./purchasing");

class User extends _sequelize.Model {
  static init(sequelize) {
    return super.init({
      id: {
        type: _sequelize.DataTypes.UUID,
        defaultValue: _sequelize.DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true
      },
      name: {
        type: _sequelize.DataTypes.STRING,
        allowNull: false
      },
      email: {
        type: _sequelize.DataTypes.STRING,
        allowNull: false
      },
      password: {
        type: _sequelize.DataTypes.STRING,
        allowNull: false
      },
      role: {
        type: _sequelize.DataTypes.ENUM(["ADMIN", "USER"]),
        defaultValue: "USER"
      },
      accessToken: {
        type: _sequelize.DataTypes.TEXT,
        allowNull: true,
        field: "access_token"
      },
      createdAt: {
        allowNull: false,
        type: _sequelize.DataTypes.DATE,
        field: "created_at",
        defaultValue: _sequelize.DataTypes.NOW
      },
      updatedAt: {
        allowNull: false,
        type: _sequelize.DataTypes.DATE,
        field: "updated_at",
        defaultValue: _sequelize.DataTypes.NOW
      },
      deletedAt: {
        allowNull: true,
        type: _sequelize.DataTypes.DATE,
        field: "deleted_at"
      }
    }, {
      sequelize: sequelize,
      tableName: "Users"
    });
  }

  static associate() {
    this.hasMany(_purchasing.Purchasing, {
      as: "userPurchasings",
      foreignKey: "user_id"
    });
  }

}

exports.User = User;