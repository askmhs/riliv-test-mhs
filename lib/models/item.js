"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Item = void 0;

var _sequelize = require("sequelize");

class Item extends _sequelize.Model {
  static init(sequelize) {
    return super.init({
      id: {
        allowNull: false,
        primaryKey: true,
        type: _sequelize.DataTypes.UUID,
        defaultValue: _sequelize.DataTypes.UUIDV4
      },
      name: {
        type: _sequelize.DataTypes.STRING,
        allowNull: false
      },
      stock: {
        type: _sequelize.DataTypes.INTEGER,
        allowNull: false
      },
      price: {
        type: _sequelize.DataTypes.BIGINT,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: _sequelize.DataTypes.DATE,
        field: "created_at",
        defaultValue: _sequelize.DataTypes.NOW
      },
      updatedAt: {
        allowNull: false,
        type: _sequelize.DataTypes.DATE,
        field: "updated_at",
        defaultValue: _sequelize.DataTypes.NOW
      },
      deletedAt: {
        allowNull: true,
        type: _sequelize.DataTypes.DATE,
        field: "deleted_at"
      }
    }, {
      sequelize: sequelize,
      tableName: "Items",
      paranoid: true
    });
  }

}

exports.Item = Item;