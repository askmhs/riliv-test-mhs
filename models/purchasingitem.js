// @flow

import { DataTypes, Model } from "sequelize";
import { Item } from "./item";
import { Purchasing } from "./purchasing";

export class PurchasingItem extends Model {
    static init(sequelize) {
        return super.init({
            id: {
                allowNull: false,
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            purchasingId: {
                type: DataTypes.UUID,
                allowNull: false,
                field: "purchasing_id"
            },
            itemId: {
                type: DataTypes.UUID,
                allowNull: false,
                field: "item_id"
            },
            qty: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            price: {
                type: DataTypes.BIGINT,
                allowNull: false
            },
            createdAt: {
                allowNull: false,
                type: DataTypes.DATE,
                field: "created_at",
                defaultValue: DataTypes.NOW
            },
            updatedAt: {
                allowNull: false,
                type: DataTypes.DATE,
                field: "updated_at",
                defaultValue: DataTypes.NOW
            },
            deletedAt: {
                allowNull: true,
                type: DataTypes.DATE,
                field: "deleted_at"
            }
        }, {
            sequelize: sequelize,
            tableName: "PurchasingItems"
        });
    }

    static associate() {
        this.belongsTo(Item, {
            foreignKey: "item_id"
        });

        this.belongsTo(Purchasing, {
            foreignKey: "purchasing_id"
        });
    }
}