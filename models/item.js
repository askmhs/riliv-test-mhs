// @flow

import { Model, DataTypes } from "sequelize";

export class Item extends Model {
    static init(sequelize) {
        return super.init({
            id: {
                allowNull: false,
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            stock: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            price: {
                type: DataTypes.BIGINT,
                allowNull: false
            },
            createdAt: {
                allowNull: false,
                type: DataTypes.DATE,
                field: "created_at",
                defaultValue: DataTypes.NOW
            },
            updatedAt: {
                allowNull: false,
                type: DataTypes.DATE,
                field: "updated_at",
                defaultValue: DataTypes.NOW
            },
            deletedAt: {
                allowNull: true,
                type: DataTypes.DATE,
                field: "deleted_at"
            }
        }, {
            sequelize: sequelize,
            tableName: "Items",
            paranoid: true
        });
    }
}