// @flow
import { Model, DataTypes } from "sequelize";
import { Purchasing } from "./purchasing";

export class User extends Model {
    static init(sequelize) {
        return super.init({
            id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                primaryKey: true,
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false
            },
            role: {
                type: DataTypes.ENUM(["ADMIN", "USER"]),
                defaultValue: "USER"
            },
            accessToken: {
                type: DataTypes.TEXT,
                allowNull: true,
                field: "access_token"
            },
            createdAt: {
                allowNull: false,
                type: DataTypes.DATE,
                field: "created_at",
                defaultValue: DataTypes.NOW
            },
            updatedAt: {
                allowNull: false,
                type: DataTypes.DATE,
                field: "updated_at",
                defaultValue: DataTypes.NOW
            },
            deletedAt: {
                allowNull: true,
                type: DataTypes.DATE,
                field: "deleted_at"
            }
        }, {
            sequelize: sequelize,
            tableName: "Users"
        });
    }

    static associate() {
        this.hasMany(Purchasing, {
            as: "userPurchasings",
            foreignKey: "user_id"
        });
    }
}