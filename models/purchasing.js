// @flow
import { Model, DataTypes } from "sequelize";
import { PurchasingItem } from "./purchasingitem";
import { User } from "./user";

export class Purchasing extends Model {
    static init(sequelize) {
        return super.init({
            id: {
                allowNull: false,
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            orderCode: {
                type: DataTypes.STRING,
                allowNull: false,
                field: "order_code"
            },
            userId: {
                type: DataTypes.UUID,
                allowNull: false,
                field: "user_id"
            },
            totalPrice: {
                type: DataTypes.BIGINT,
                allowNull: false,
                field: "total_price"
            },
            status: {
                type: DataTypes.ENUM(["PENDING", "PAID", "COMPLETED", "REJECTED"]),
                defaultValue: "PENDING"
            },
            receipt: {
                type: DataTypes.STRING,
                allowNull: true
            },
            createdAt: {
                allowNull: false,
                type: DataTypes.DATE,
                field: "created_at",
                defaultValue: DataTypes.NOW
            },
            updatedAt: {
                allowNull: false,
                type: DataTypes.DATE,
                field: "updated_at",
                defaultValue: DataTypes.NOW
            },
            deletedAt: {
                allowNull: true,
                type: DataTypes.DATE,
                field: "deleted_at"
            }
        }, {
            sequelize: sequelize,
            tableName: "Purchasings"
        });
    }

    static associate() {
        this.hasMany(PurchasingItem, {
            as: "purchasingItems",
            foreignKey: "purchasing_id",
            hooks: true
        });

        this.belongsTo(User, {
            foreignKey: "user_id"
        });
    }
}