// @flow
import { Request, Response } from "express";
import { ItemService } from "../services/ItemService";
import { ResponseBuilder } from "../utils/ResponseBuilder";

const itemService = new ItemService();
export class ItemController {
    async index(req: Request, res: Response): Promise<Response> {
        try {
            const result = await itemService.listItem(req.query.q);
            res.status(200).json(ResponseBuilder.successResponse("List item", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }

    async show(req: Request, res: Response): Promise<Response> {
        try {
            const result = await itemService.detailItem(req.params.id);
            res.status(200).json(ResponseBuilder.successResponse("Detail item", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }

    async store(req: Request, res: Response): Promise<Response> {
        try {
            const result = await itemService.createNewItem(req.body);
            res.status(200).json(ResponseBuilder.successResponse("Yeay!, Successfully create a new item", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }

    async update(req: Request, res: Response): Promise<Response> {
        try {
            const result = await itemService.updateItem(req.params.id, req.body);
            res.status(200).json(ResponseBuilder.successResponse("Yeay!, Successfully update an item", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }

    async destroy(req: Request, res: Response): Promise<Response> {
        try {
            const result = await itemService.deleteItem(req.params.id);
            res.status(200).json(ResponseBuilder.successResponse("Yeay!, Successfully delete an item", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }
}