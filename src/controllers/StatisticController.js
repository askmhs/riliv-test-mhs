// @flow
import { Request, Response } from "sequelize";
import { StatisticService } from "../services/StatisticService";
import { ResponseBuilder } from "../utils/ResponseBuilder";

const statisticService = new StatisticService();

export class StatisticController {
    async soldItem(req: Request, res: Response): Promise<Response> {
        try {
            const { sort = "asc", startDate, endDate } = req.query;

            const result = await statisticService.soldItem(sort, startDate, endDate);
            res.status(200).json(ResponseBuilder.successResponse("Sold item statistic", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }

    async loyalCustomer(req: Request, res: Response): Promise<Response> {
        try {
            const { startDate, endDate } = req.query;
            const result = await statisticService.loyalCustomer(startDate, endDate);
            res.status(200).json(ResponseBuilder.successResponse("Loyal customer statistic", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }

    async income(req: Request, res: Response): Promise<Response> {
        try {
            const { startDate, endDate } = req.query;
            const result = await statisticService.income(startDate, endDate);
            res.status(200).json(ResponseBuilder.successResponse("Income statistic", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }
}