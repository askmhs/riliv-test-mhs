// @flow
import { Request, Response } from "express";
import { UserService } from "../services/UserService";
import { ResponseBuilder } from "../utils/ResponseBuilder";

const userService = new UserService();
export class UserController {
    async signUp(req: Request, res: Response): Promise<Response> {
        try {
            const result = await userService.createNewUser(req.body);
            res.status(200).json(ResponseBuilder.successResponse("Yeay!, You're successfully registered", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }

    async login(req: Request, res: Response): Promise<Response> {
        try {
            const { email, password } = req.body;
            const result = await userService.loginUser(email, password);
            res.status(200).json(ResponseBuilder.successResponse("Yeay!, You're successfully logged in", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }
}