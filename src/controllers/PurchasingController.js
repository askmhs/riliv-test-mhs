// @flow
import { Request, Response } from "express";
import { PurchasingService } from "../services/PurchasingService";
import { ResponseBuilder } from "../utils/ResponseBuilder";

const purchasingService = new PurchasingService();

export class PurchasingController {
    async index(req: Request, res: Response): Promise<Response> {
        try {
            const { id: userId, role: userRole } = req.user;
            const result = await purchasingService.listPurchase(req.query, userId, userRole);
            res.status(200).json(ResponseBuilder.successResponse("List purchase", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }

    async show(req: Request, res: Response): Promise<Response> {
        try {
            const { id: userId, role: userRole } = req.user;
            const result = await purchasingService.detailPurchase(req.params.id, userId, userRole);
            res.status(200).json(ResponseBuilder.successResponse("Detail purchase", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }

    async store(req: Request, res: Response): Promise<Response> {
        try {
            const result = await purchasingService.createNewPurchase(req.body, req.user);
            res.status(200).json(ResponseBuilder.successResponse("Yeay!, Successfully create a new purchase", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }

    async uploadReceipt(req: Request, res: Response): Promise<Response> {
        try {
            const result = await purchasingService.uploadReceipt(req.params.id, req.files.receipt, req.user.id);
            res.status(200).json(ResponseBuilder.successResponse("Yeay!, Successfully upload receipt", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }

    async setPurchaseStatus(req: Request, res: Response): Promise<Response> {
        try {
            const result = await purchasingService.setStatus(req.params.id, req.body.status);
            res.status(200).json(ResponseBuilder.successResponse("Yeay!, Successfully update purchase status", result));
        } catch (error) {
            console.log(error);
            res.status(500).json(ResponseBuilder.errorResponse(error.message));
        }
    }
}