// @flow

import express from "express";
import cors from "cors";
import routers from "./routers";
import DB from "./../models";
import bodyParser from "body-parser";
import { User } from "../models/user";
import { Item } from "../models/item";
import { Purchasing } from "../models/purchasing";
import { PurchasingItem } from "../models/purchasingitem";
import fileUpload from "express-fileupload";

const app = express();

app.use(fileUpload());
app.use(bodyParser.json());
app.use(routers);
app.use(cors());

DB.sequelize.authenticate().then(async () => {
    User.init(DB.sequelize);
    Item.init(DB.sequelize);
    Purchasing.init(DB.sequelize);
    PurchasingItem.init(DB.sequelize);

    Purchasing.associate();
    PurchasingItem.associate();

    const port = process.env.PORT || 3000;

    app.listen(port, () => {
        console.log(`Express server listening on port ${port}.`);
    });
}).catch((error) => {
    throw error;
});