// @flow
import express from "express";
import { StatisticController } from "../controllers/StatisticController";
import { authenticateAdmin } from "../middlewares/authentication";

const router = express.Router();
const statisticController = new StatisticController();

router.get("/sold-item", authenticateAdmin, statisticController.soldItem);
router.get("/loyal-customer", authenticateAdmin, statisticController.loyalCustomer);
router.get("/income", authenticateAdmin, statisticController.income);

module.exports = router;