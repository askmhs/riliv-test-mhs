// @flow

import express from "express";
import itemsRouter from "./items.route";
import userRouter from "./users.route";
import purchasingRouter from "./purchasings.route";
import statisticRouter from "./statistics.route";

const router = express.Router();

router.use("/items", itemsRouter);
router.use("/users", userRouter);
router.use("/purchasings", purchasingRouter);
router.use("/statistics", statisticRouter);

module.exports = router;