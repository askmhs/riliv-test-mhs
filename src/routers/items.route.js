// @flow
import express from "express";
import { ItemController } from "../controllers/ItemController";
import { authenticateAdmin } from "./../middlewares/authentication";

const router = express.Router();
const itemController = new ItemController();

router.get("/", itemController.index);
router.get("/:id", itemController.show);
router.post("/", authenticateAdmin, itemController.store);
router.put("/:id", authenticateAdmin, itemController.update);
router.delete("/:id", authenticateAdmin, itemController.destroy);

module.exports = router;