// @flow
import express from "express";
import { PurchasingController } from "../controllers/PurchasingController";
import { authenticate, authenticateAdmin, authenticateUser } from "../middlewares/authentication";

const router = express.Router();
const purchasingController = new PurchasingController();

router.get("/", authenticate, purchasingController.index);
router.post("/", authenticateUser, purchasingController.store);
router.post("/:id/receipt", authenticateUser, purchasingController.uploadReceipt);
router.patch("/:id/status", authenticateAdmin, purchasingController.setPurchaseStatus)

module.exports = router;
