// @flow
import { User } from "../../models/user";
import bcrypt from "bcrypt";
import { Token } from "../utils/Token";

export class UserService {
    _token: Token;

    constructor() {
        this.token = new Token();
    }

    get token(): Token {
        return this._token;
    }

    set token(value: Token) {
        this._token = value;
    }

    async createNewUser(data: { [key: string]: any }): Promise<{ [key: string]: any }> {
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(data.password, salt);

        data.password = hashedPassword;

        const user: User = new User(data);
        const { id, name, role } = user;
        const accessToken = await this.token.encode({ id, name, role });

        user.accessToken = accessToken;
        await user.save();

        return { accessToken };
    }

    async loginUser(email: string, password: string): Promise<{ [key: string]: any }> {
        const user: User = await User.findOne({ where: { email }});

        if (!user) {
            throw new Error("Whoops!, Invalid email or password");
        }

        const isPasswordValid: boolean = await bcrypt.compare(password, user.password);
        if (!isPasswordValid) {
            throw new Error("Whoops!, Invalid email or password");
        }

        const { id, name, role } = user;
        const accessToken = await this.token.encode({ id, name, role });

        user.accessToken = accessToken;
        await user.save();

        return { accessToken };
    }
}