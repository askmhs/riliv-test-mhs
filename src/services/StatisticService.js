import { Op, Sequelize } from "sequelize";
import { PurchasingItem } from "../../models/purchasingitem";
import moment from "moment";
import * as _ from "lodash";
import { Item } from "../../models/item";
import { Purchasing } from "../../models/purchasing";
import { User } from "../../models/user";

export class StatisticService {
    _sortType: string[];

    constructor() {
        this.sortType = ["asc", "desc"];
    }

    get sortType(): string[] {
        return this._sortType;
    }

    set sortType(value: string[]) {
        this._sortType = value;
    }

    async soldItem(sort: string, startDate: string, endDate: string): Promise<{ [key: string]: string }> {
        if (!this.sortType.includes(sort)) {
            throw new Error(`Whoops!, ${sort} is not a valid param`);
        }

        if (!startDate) {
            startDate = moment().startOf("day").toISOString();
        } else {
            startDate = moment(startDate).startOf("day").toISOString();
        }
        
        if (!endDate) {
            endDate = moment().endOf("day").toISOString();
        } else {
            endDate = moment(endDate).endOf("day").toISOString();
        }

        const result = await PurchasingItem.findAll({
            attributes: ["PurchasingItem.item_id", [Sequelize.fn("sum", Sequelize.col("PurchasingItem.qty")), "qty"]],
            where: { createdAt: { [Op.between]: [startDate, endDate] }, "$Purchasing.status$": { [Op.not]: "REJECTED" } },
            include: [
                {
                    model: Item,
                    attributes: ["id", "name"]
                },
                {
                    model: Purchasing,
                    attributes: []
                }
            ],
            group: ["PurchasingItem.item_id", "Item.id", "Item.name"]
        });

        return _.chain(result)
            .orderBy("qty", sort)
            .map((object) => {
                return {
                    qty: object.qty,
                    id: object.Item.id,
                    name: object.Item.name
                };
            })
            .value();
    }

    async loyalCustomer(startDate: string, endDate: string): Promise<{ [key: string]: string }> {
        if (!startDate) {
            startDate = moment().startOf("day").toISOString();
        } else {
            startDate = moment(startDate).startOf("day").toISOString();
        }
        
        if (!endDate) {
            endDate = moment().endOf("day").toISOString();
        } else {
            endDate = moment(endDate).endOf("day").toISOString();
        }

        const result = await Purchasing.findAll({
            where: { createdAt: { [Op.between]: [startDate, endDate] }, status: { [Op.not]: "REJECTED" } },
            attributes: ["Purchasing.user_id", [Sequelize.fn("COUNT", Sequelize.col("Purchasing.user_id")), "orderCount"]],
            include: {
                model: User,
                attributes: ["id", "name"]
            },
            group: ["Purchasing.user_id", "User.id", "User.name"]
        });

        return _.chain(result)
            .orderBy("orderCount", "desc")
            .map((object) => {
                object = object.toJSON();

                return {
                    id: object.User.id,
                    name: object.User.name,
                    orderCount: object.orderCount
                }
            })
            .value();
    }

    async income(startDate: String, endDate: string): Promise<{ [key: string]: string }> {
        const result = await Purchasing.findAll({
            where: { createdAt: { [Op.between]: [startDate, endDate] }, status: { [Op.not]: "REJECTED" } },
            attributes: [[Sequelize.fn("sum", Sequelize.col("Purchasing.total_price")), "totalIncome"]]
        });

        return {
            period: `${startDate} - ${endDate}`,
            result
        };
    }
}