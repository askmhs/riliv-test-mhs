// @flow

import { Op } from "sequelize";
import { Item } from "../../models/item";

export class ItemService {
    async createNewItem(data: { [key: string]: any }): Promise<Item> {
        return Item.create(data);
    }

    async listItem(searchKey: string): Promise<Item[]> {
        let params = {};

        if (searchKey) {
            Object.assign(params, {
                name: { [Op.like]: `%${searchKey}%` }
            });
        }

        return Item.findAll({
            where: params,
            order: [
                ["createdAt", "DESC"]
            ]
        });
    }

    async detailItem(id: string): Promise<Item> {
        return Item.findByPk(id);
    }

    async updateItem(id: string, data: { [key: string]: any }): Promise<Item> {
        const item: Item = await Item.findByPk(id);

        if (!item) {
            throw new Error(`Whoops!, product with ID ${id} couldn't be found`);
        }

        await item.update(data);

        return this.detailItem(id);
    }

    async deleteItem(id: string) {
        const item: Item = await Item.findByPk(id);

        if (!item) {
            throw new Error(`Whoops!, product with ID ${id} couldn't be found`);
        }

        return item.destroy();
    }
}