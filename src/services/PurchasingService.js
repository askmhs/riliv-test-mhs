import { status } from "express/lib/response";
import { Op } from "sequelize";
import { sequelize } from "../../models";
import { Item } from "../../models/item";
import { Purchasing } from "../../models/purchasing";
import { PurchasingItem } from "../../models/purchasingitem";

// @flow
export class PurchasingService {
    async listPurchase(
        queries: { [key: string]: any },
        userId: string,
        userRole: string
    ): Promise<Purchasing[]> {
        const { q, page = 1, perPage = 10 } = queries;
        const params = {};

        if (userRole === "USER") {
            Object.assign(params, { userId });
        }

        if (q) {
            Object.assign(params, {
                orderCode: { [Op.like]: `%${searchKey}%` },
            });
        }

        const offset = (page - 1) * perPage;
        const limit = perPage;

        return Purchasing.findAll({
            where: params,
            order: [
                ["createdAt", "DESC"]
            ],
            offset,
            limit
        });
    }

    async detailPurchase(id: string, userRole: string, userId: string): Promise<Purchasing> {
        const params = { id };

        if (userRole === "USER") {
            Object.assign(params, { userId });
        }

        const purchase: Purchasing = await Purchasing.findOne({
            where: params,
            include: {
                association: "purchasingItems"
            }
        });

        if (!purchase) {
            throw new Error(`Whoops!, The purchase with ID ${id} couldn't be found!`);
        }

        return purchase;
    }

    async createNewPurchase(
        data: { [key: string]: any },
        user: { [key: string]: any }
    ): Promise<Purchasing> {
        const { items } = data;
        const { id: userId } = user;
        const orderCode: string = `PUR-${new Date().getTime()}`;

        return sequelize.transaction(async () => {
            const purchasing = new Purchasing({ userId, orderCode });
            let totalPrice = 0;

            for (let item of items) {
                const validItem: Item = await this.validateItem(item.id);

                await PurchasingItem.create({
                    itemId: item.id,
                    purchasingId: purchasing.id,
                    qty: item.qty,
                    price: validItem.price
                });

                validItem.stock -= item.qty;
                await validItem.save();

                totalPrice += item.qty * validItem.price;
            }

            purchasing.totalPrice = totalPrice;
            await purchasing.save();

            return Purchasing.findByPk(purchasing.id, {
                include: {
                    association: "purchasingItems"
                }
            });
        });
    }

    async validateItem(id: string, qty: number): Promise<Item> {
        const item: Item = await Item.findByPk(id);

        if (!item) {
            throw new Error(`Whoops!, Item with the ID ${id} couldn't be found`);
        }

        if (item && item.qty < qty) {
            throw new Error(`Whoops!, Item with the ID ${id} didn't have enough stock`);
        }

        return item;
    }

    async uploadReceipt(id: string, receipt: any, userId: string): Promise<Purchasing> {
        const purchasing = await Purchasing.findOne({
            where: {
                id,
                userId,
                status: "PENDING"
            }
        });

        if (!purchasing) {
            throw new Error(`Purchase with ID ${id} couldn't be found!`);
        }

        const splitName: string[] = receipt.name.split(".");
        const extension: string = splitName[splitName.length - 1];
        const fileName: string = `uploads/${new Date().getTime()}.${extension}`;
        const path: string = `${__dirname}/../../${fileName}`;
        await receipt.mv(path);

        purchasing.receipt = fileName;
        await purchasing.update();

        return purchasing;
    }

    async setStatus(orderId: string, status: string): Promise<Purchasing> {
        const allowedStatus: string[] = ["PAID", "COMPLETED", "REJECTED"];

        if (!allowedStatus.includes(status)) {
            throw new Error(`Whoops!, You are not allowed to set purchase status to ${status}`);
        }

        const purchase: Purchasing = await Purchasing.findByPk(orderId);
        
        if (!purchase) {
            throw new Error(`Whoops!, The purchase data couldn't be found`);
        }

        purchase.status = status.toUpperCase();
        await purchase.update();

        return purchase;
    }
}