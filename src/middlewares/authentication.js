// @flow

import { ResponseBuilder } from "../utils/ResponseBuilder";
import { Request, Response, NextFunction } from "express";
import { Token } from "../utils/Token";

const token = new Token();

export const authenticate = (req: Request, res: Response, next: NextFunction) => {
    const accessToken = req.headers.token;

    if (!accessToken) {
        return res.status(401).json(
            ResponseBuilder.errorResponse("Whoops!, authentication token is missing")
        );
    }

    try {
        const decodedToken = token.decode(accessToken);
        req.user = decodedToken;

        next();
    } catch (error) {
        console.log(error);
        return res.status(401).json(
            ResponseBuilder.errorResponse("Whoops!, you are not allowed to access this feature")
        );
    }
}

export const authenticateUser = (req: Request, res: Response, next: NextFunction) => {
    const accessToken = req.headers.token;

    if (!accessToken) {
        return res.status(401).json(
            ResponseBuilder.errorResponse("Whoops!, authentication token is missing")
        );
    }

    try {
        const decodedToken = token.decode(accessToken);

        if (decodedToken.role !== "USER") {
            return res.status(401).json(
                ResponseBuilder.errorResponse("Whoops!, you are not allowed to access this feature")
            );
        }

        req.user = decodedToken;

        next();
    } catch (error) {
        console.log(error);
        return res.status(401).json(
            ResponseBuilder.errorResponse("Whoops!, you are not allowed to access this feature")
        );
    }
}

export const authenticateAdmin = (req: Request, res: Response, next: NextFunction) => {
    const accessToken = req.headers.token;

    if (!accessToken) {
        res.status(401).json(
            ResponseBuilder.errorResponse("Whoops!, authentication token is missing")
        );
    }

    try {
        const decodedToken = token.decode(accessToken);

        if (decodedToken.role !== "ADMIN") {
            return res.status(401).json(
                ResponseBuilder.errorResponse("Whoops!, you are not allowed to access this feature")
            );
        }

        req.user = decodedToken;

        next();
    } catch (error) {
        console.log(error);
        return res.status(401).json(
            ResponseBuilder.errorResponse("Whoops!, you are not allowed to access this feature")
        );
    }
}