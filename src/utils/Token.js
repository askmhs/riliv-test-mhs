// @flow
import jwt from "jsonwebtoken";

export class Token {
    _privateKey: string;

    constructor() {
        this.privateKey = process.env.JWT_PRIVATE_KEY || "riliv-test";
    }

    get privateKey(): string {
        return this._privateKey;
    }

    set privateKey(value: string) {
        this._privateKey = value;
    }

    async encode(data: { [key: string]: any }): Promise<string> {
        return jwt.sign(data, this.privateKey, {
            algorithm: "HS256",
            expiresIn: "1h"
        });
    }

    decode(token: string): Promise<{ [key: string]: any}> {
        return jwt.verify(token, this.privateKey, {
            algorithms: "HS256"
        });
    }
}