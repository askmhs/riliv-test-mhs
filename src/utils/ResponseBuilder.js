// @flow

export class ResponseBuilder {
    static successResponse(message: string, data: { [key: string]: any }): { [key: string]: any } {
        return {
            success: true,
            message,
            data
        };
    }

    static errorResponse(errors: any): { [key: string]: any } {
        return {
            success: false,
            message: "Whoops!, An error occurred.",
            errors
        };
    }
}